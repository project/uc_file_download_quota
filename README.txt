UC File Download Limit
uc_file add on module
 The purpose of this module is to :
 1. Provide Download button that is not related to cart
 2. User who has X role can download X time per X duration (day/week/month/year)
 3. User who doesn't have 'access file quota' is excluded and cannot use the 
    download button
 4. The download counting is based on per product SKU.
 5. User can still download past registered product download regardless of 
    current download limit.
 6. Multiple product per SKU will be recorded to user user/X/purchased-files and 
    the latest version is fetched for download
 Future version will add these feature if anyone want to finance 
 the development cost :
 1. Able to limit / configure download per roles and per sku
 2. Create this as product feature
 3. Alter the add to cart button, eliminate the need to manually add the button

Future additional features will only be added if this module is 
approved as a drupal.org project. 
