<?php
/**
 * @file
 * uc_file_download_quota module file
 */

/**
 * Implements hook_help().
 */
function uc_file_download_quota_help($section) {
  switch ($section) {
    case 'admin/help#uc_file_download_quota':
      $output = t("uc file download quota is a add-on module for uc_file which enable admin to limit product download
                 Per role and per time limit");
      return $output;

    case 'admin/modules#description':
      return t('file download quota is a add-on module for uc_file which enable admin to limit product download
                 Per role and per time limit');
  }
}

/**
 * Implements hook_perm().
 */
function uc_file_download_quota_perm() {
  return array('administer ubercart file quota', 'access file quota');
}

/**
 * Implements hook_menu().
 * we need to open a menu for ajax callback
 */
function uc_file_download_quota_menu() {
  $items = array();
  $items['admin/settings/uc_file_download_quota'] = array(
    'title' => 'Ubercart File Quota',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_file_download_quota_configuration_form'),
    'access arguments' => array('administer ubercart file quota'),
  );
  return $items;
}


/**
 * Form for configuring the module configuration options
 */
function uc_file_download_quota_configuration_form() {
  $form['ddr_basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('File Quota Setting'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -19,
  );
  $form['ddr_basic']['uc_file_download_quota_download_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the maximum download limit per given time'),
    '#default_value' => variable_get('uc_file_download_quota_download_limit', '20'),
  );
  $form['ddr_basic']['uc_file_download_quota_anonymous_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Anonymous user redirect'),
    '#default_value' => variable_get('uc_file_download_quota_anonymous_redirect', 'membership-plans'),
  );
  $form['ddr_basic']['uc_file_download_quota_no_role_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Member without role redirect'),
    '#default_value' => variable_get('uc_file_download_quota_no_role_redirect', 'membership-plans'),
  );
  $form['ddr_basic']['uc_file_download_quota_download_limit_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Download limit reached redirect'),
    '#default_value' => variable_get('uc_file_download_quota_download_limit_redirect', 'download-limit'),
  );

  $form['ddr_role'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collasped' => FALSE,
    '#title' => t('Limit Configuration'),
    '#theme' => 'uc_file_download_quota_limit_form',
  );
  $roles = user_roles($membersonly = TRUE, $permission = 'access file quota');
  $options = range(0, variable_get('uc_file_download_quota_download_limit', '20'));
  $options['unlimited'] = t('Unlimited');

  $month = array(
  	'1' => t('Daily'),
  	'7' => t('Weekly'),
  	'30' => t('Monthly'),
  	'90' => t('Quarterly'),
  	'360' => t('Yearly')
  );

  foreach ($roles as $key => $role) {
    $form['ddr_role']['uc_file_download_quota_download_role_' . $key] = array(
      '#type' => 'value',
      '#value' => $role,
    );
    $form['ddr_role']['uc_file_download_quota_download_' . $key] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => variable_get('uc_file_download_quota_download_' . $key, '4'),
      '#description' => t('Set to unlimited to remove the download limit'),
    );

    $form['ddr_role']['uc_file_download_quota_time_' . $key] = array(
      '#type' => 'select',
      '#options' => $month,
      '#default_value' => variable_get('uc_file_download_quota_time_' . $key, '30'),
    );
  }
  return system_settings_form($form);
}


/**
 * Check if user still has quota to download
 * return 1 if the user has no acceptable role
 * return 2 if the user quota is depleted
 * return 3 if user still has quota for download
 */
function uc_file_download_quota_check_access() {
  // Unlimited user fix.
  global $user;
  $timenow = time();
  $roles = user_roles($membersonly = TRUE, $permission = 'access file quota');

  // We count how many days since user registered.
  $date_difference = floor(($timenow - $user->created) / (60 * 60 * 24));

  $days = array();
  foreach ($roles as $key => $role) {
    if (is_array($user->roles) && in_array($role, array_values($user->roles))) {
      $limit = variable_get('uc_file_download_quota_download_' . $key, '4');

      // Fix for unlimited limit, skip everything - 'unlimited download' set.
      if ($limit == 'unlimited') {
        return '3';
      }

      // No limit roles detected skip loop and begin next loop.
      if ($limit == 0) {
        continue;
      }

      $total_quota += $limit;

      // Build the configured days.
		  $days[] = variable_get('drupie_download_role_time_'. $key, '30');
    }
  }

  // Subscription start data for user.
  $subsql = "SELECT u.next_charge, u.created
  					FROM {uc_recurring_users} u
  					WHERE u.uid=%d
  					ORDER BY u.created DESC";

  $subquery = db_query_range($subsql, $user->uid, 0, 1);
  $subdata = db_fetch_object($subquery);

  // Check if the user subscription is still valid.
  if (isset($subdata->next_charge) && $subdata->next_charge >= $timenow) {
    // Logic to convert timestamp to daily
    // basis for start and end date.
    $allowed_time = (60 * 60 * 24) * max($days);
    $time_left = ($timenow - $subdata->created) / $allowed_time;
    $startdate = $subdata->created + (floor($time_left) * $allowed_time);
    $enddate = $subdata->created + (ceil($time_left) * $allowed_time);

    $sql = "SELECT d.fid, d.timestamp
    				FROM {uc_file_download_quota} d
    				WHERE d.uid = %d";

    $query = db_query($sql, $user->uid);
    while ($data = db_fetch_object($query)) {
      if (($startdate <= $data->timestamp) && ($data->timestamp <= $enddate)) {
        $downloaded[$data->fid] = $data->fid;
      }
    }
    $total_downloaded = count($downloaded);
  }

  // Select differnt return value based
  // on user subscription condition.
  if ($user->uid == 1) {
    return '3';
  }

  if (empty($total_quota) || $subdata->next_charge <= $timenow || empty($subdata)) {
    return '1';
  }
  elseif ($total_downloaded >= $total_quota) {
    return '2';
  }
  elseif ($total_downloaded < $total_quota) {
    return '3';
  }
}

/**
 * Function to check if current product model already on the download list
 */
function uc_file_download_quota_check_model($model, $uid) {
  $sql = "SELECT COUNT(d.fid) AS count FROM {uc_file_download_quota} d WHERE d.uid = %d AND d.fid = '%s'";
  $query = db_fetch_object(db_query($sql, $uid, $model));
  return $query->count;
}

/**
 * The download button form
 * Need the product SKU / model number for this form to work
 */
function uc_file_download_quota_download_button_form($form_state, $model) {
  $form['sku'] = array(
    '#type' => 'hidden',
    '#value' => $model,
  );
  $form['download'] = array(
    '#type' => 'submit',
    '#value' => 'Download',
    '#name' => $model,
  );

  return $form;

}

/**
 * The form submit function
 */
function uc_file_download_quota_download_button_form_submit($form, &$form_state) {
  // We need to include the uc_file.pages.
  module_load_include('inc', 'uc_file', 'uc_file.pages');

  global $user;
  $exists = uc_file_download_quota_check_model($form_state['clicked_button']['#post']['sku'], $user->uid);

  // Calculate the limits.
  $limit = 3;
  if (empty($exists)) {
    $limit = uc_file_download_quota_check_access();
  }

  switch ($limit) {
    case 1:
      $redirect = variable_get('uc_file_download_quota_anonymous_redirect', 'membership-plans');
      if ($user->uid > 0) {
        $redirect = variable_get('uc_file_download_quota_no_role_redirect', 'membership-plans');
      }

      // Redirect user to configured page.
      drupal_goto($redirect);
      break;

    case 2:
      drupal_goto(variable_get('uc_file_download_quota_download_limit_redirect', 'download-limit'));
      break;

    case 3:
      // Prepare file download.
      $model = $form_state['clicked_button']['#post']['sku'];

      $sql = "SELECT ucf.fid , ucf.pfid
      				FROM {uc_file_products} AS ucf
      				LEFT JOIN {uc_files} AS f
      				ON f.fid = ucf.fid
      				WHERE ucf.model = '%s'
      				ORDER BY f.fid DESC";

      $result = db_query($sql, $model);

      // Assuming file is always new, check is in the form function.
      $download_modification['download_limit'] = variable_get('uc_file_download_limit_number', NULL);
      $download_modification['address_limit'] = variable_get('uc_file_download_limit_addresses', NULL);

      $options = array(
        'time_polarity' => '+',
        'time_quantity' => variable_get('uc_file_download_limit_duration_qty', NULL),
        'time_granularity' => variable_get('uc_file_download_limit_duration_granularity', 'never'),
      );

      $download_modification['expiration'] = _uc_file_expiration_date($options, time());

      // Build the files array and add the files to user download page.
      while ($data = db_fetch_object($result)) {
        $files[] = $data->fid;
        uc_file_user_renew($data->fid, $user, $data->pfid, $download_modification, TRUE);
      }

      // Record download to database if there are no existing database entry.
      if (empty($exists)) {
        $record->fid = $model;
        $record->uid = $user->uid;
        $record->timestamp = time();

        drupal_write_record('uc_file_download_quota', $record);
      }

      // Fetch the latest file and send it to user.
      $sql = "SELECT fid, file_key FROM {uc_file_users} WHERE fid = '%s' AND uid = %d";
      $result = db_fetch_array(db_query($sql, $files[0], $user->uid));
      _uc_file_download($result['fid'], $result['file_key']);

      break;
  }
}

/**
 * Implements hook_theme().
 */
function uc_file_download_quota_theme() {
  return array(
    'uc_file_download_quota_limit_form' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Administration page theme table
 */
function theme_uc_file_download_quota_limit_form($form) {
  $headers = array(t('Roles'), t('Download Limit'), t('Duration'));
  $roles = user_roles($membersonly = TRUE, $permission = 'access file quota');

  foreach ($roles as $key => $value) {
    $rows[] = array(
      'data' => array(
        array('data' => $form['uc_file_download_quota_download_role_' . $key]['#value']),
        array('data' => drupal_render($form['uc_file_download_quota_download_' . $key])),
        array('data' => drupal_render($form['uc_file_download_quota_time_' . $key])),
      ),
    );
  }

  if (empty($roles)) {
    $rows[] = array(
      'data' => array(
        array(
        	'data' => t("You need to assign the 'access file quota' permission to
        							 the user roles that you allow to access the download button
        							 before this module can work"),
        	'colspan' => 3,
        ),
      ),
    );
  }

  return theme('table', $headers, $rows);
}

/**
 * Implements hook_user_delete().
 */
function uc_file_download_quota_user_delete($edit, $uid) {
  db_query("DELETE FROM {uc_file_download_quota} WHERE uid = %d", $uid);
}

/**
 * Implements hook cron().
 */
function uc_file_download_quota_cron() {
  $result = db_query("SELECT DISTINCT(model) FROM {uc_files_products}");
  $files = db_fetch_array($result);

  $user_result = db_query("SELECT DISTINCT(fid) FROM {uc_file_download_quota}");
  $user_files = db_fetch_array($user_result);

  foreach ($user_files as $key => $values) {
    if (!in_array($values, $files)) {
      db_query("DELETE FROM {uc_file_download_quota} WHERE fid = %d", $values);
    }
  }
}

/**
 * Function to delete recorded based on order
 *
 * @param $order
 *   ubercart order object, usually found in conditional action
 */
function uc_file_download_quota_remove_files($order) {
  // Load user.
  if (!$order->uid) {
    return FALSE;
  }

  // Delete the record.
  db_query("DELETE FROM {drupie_download_role} WHERE uid = %d", $order->uid);
  db_query("DELETE FROM {uc_file_users} WHERE uid = %d", $order->uid);
}

/**
 * Implementats hook_ca_action().
 */
function uc_file_download_quota_ca_action() {
  $actions['uc_file_download_quota_remove_files'] = array(
    '#title' => t('Remove download access'),
    '#category' => t('File Quota Removal'),
    '#callback' => 'uc_file_download_quota_remove_files',
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order'),
      ),
    ),
  );

  return $actions;
}
